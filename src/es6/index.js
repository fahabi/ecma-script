function newFunction(name, age, country) {
    var name = name || 'Fabian Ramirez';
    var age = age || 24;
    var country = country || 'AR';

    console.log(name, age, country);
}

//es6
function newFunction2(name = 'Fabian Ramirez', age = 24, country = 'AR') {
    console.log(name, age, country);
}

newFunction2();
newFunction2('Fabii', 23, 'AR');

let hello = 'Hello';
let world = 'World';
let epicPhrase = hello + ' ' + world;
console.log(epicPhrase);
let epicPhrase2 = `${hello} ${world}`;
console.log(epicPhrase2);

let lorem = 'Hola \n' + 'como estas?';

//es6
let lorem2 = `Otra frase epica
otra frase
y otra`;

console.log(lorem);
console.log(lorem2);

//Object person
let person = {
    'name': 'Fabian',
    'age': 24,
    'country': 'AR',
}

console.log(person.name, person.age);

//es6
let { name, age } = person;
console.log(name, age);

let team1 = ['fabi', 'mati'];
let team2 = ['flor', 'nelson'];

let education = ['erik', ...team1, ...team2];
console.log(education);


//asignacion de Object 
let name = 'Fabian';
let age = 24;

obj = { name: name, age: age };

//es6
obj2 = { name, age };

console.log(obj2);

//arrow functions
const names = [
    { name: 'fabi', age: 24 },
    { name: 'mati', age: 27 },
];

let listOfNames = names.map(function(item) {
    console.log(item.name);
})

let listOfNames2 = names.map(item => console.log(item.name));

const listOfNames3 = (name, age) => {
    //contenido
}

const listOfNames4 = name => {
    //contenido
}

const square = num => num * num;

//promesas
const helloPromise = () => {
    return new Promise((resolve, reject) => {
        if (true) {
            resolve('Hey');
        } else {
            reject('Ups!');
        }
    });
}

helloPromise()
    .then(response => console.log(response))
    .then(() => console.log('hola'))
    .catch(error => console.log(error));


//CLASS
class calculator {
    constructor() {
        this.valueA = 0;
        this.valueB = 0;
    }

    sum(valueA, valueB) {
        this.valueA = valueA;
        this.valueB = valueB;
        return this.valueA + this.valueB;
    }
}

const calc = new calculator();
console.log(calc.sum(2, 2));