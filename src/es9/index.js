const obj = {
    'name': 'Fabian',
    'age': 24,
    'country': 'AR',
}

let { name, ...all } = obj;
console.log(name, all);


//unir dos object
const obj = {
    'name': 'Fabian',
    'age': 24,
}
const obj2 = {
    ...obj,
    'country': 'AR',
}
console.log(obj2);


// se agrega finally para poder saber cuando termina una promesa
const helloWorld = () => {
    return new Promise((resolve, reject) => {
        (true) ?
        setTimeout(() => resolve('Hello world'), 3000): reject(new Error('Test Error'))
    });
};

helloWorld()
    .then(response => console.log(response))
    .catch(error => console.log(error))
    .finally(() => {
        console.log('finalizo');
    })


//manejo de regex
const regexData = /([0-9]{4})-([0-9]{2})-([0-9]{2})/
const match = regexData.exec('2020-04-04');

const year = match[1];
const month = match[2];
const day = match[3];

console.log(year, month, day);