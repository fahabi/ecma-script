const data = {
    frontend: 'fabian',
    backend: 'matias',
    QA: 'erik',
};

//convertir object a matriz
const entries = Object.entries(data);

console.log(entries);



//array de string
const data = {
    frontend: 'fabian',
    backend: 'matias',
    QA: 'erik',
};

//convertir object a matriz
const entries = Object.values(data);

console.log(entries);


//padding
const string = 'hello';
console.log(string.padStart(7, '_'));
console.log(string.padEnd(12, '_'));


//Async Await
const helloWorld = () => {
    return new Promise((resolve, reject) => {
        (true) ?
        setTimeout(() => resolve('Hello world'), 3000): reject(new Error('Test Error'))
    })
}

const helloAsync = async() => {
    const hello = await helloWorld();
    console.log(hello);
}

helloAsync();

const anotherFunction = async() => {
    try {
        const hello = await helloWorld();
        console.log(hello);
    } catch (error) {
        console.log(error);
    }
}

anotherFunction();